# make-api-tests

This is a project combining make-api-fe and make-api-be-fetcher projects. It is used for testing the functionality of the MakeAPI project.

## Prerequisites

Project contains submodules [make-api-fe](https://gitlab.fit.cvut.cz/vanisond/make-api-fe) and [make-api-be-fetcher](https://gitlab.fit.cvut.cz/vanisond/make-api-be-fetcher), 
so you need to clone it with `--recurse-submodules` flag.

```bash
git clone --recurse-submodules git@gitlab.fit.cvut.cz:vanisond/make-api-tests.git
```

Or you can clone it without submodules and then initialize them.

```bash
git clone git@gitlab.fit.cvut.cz:vanisond/make-api-tests.git
git submodule init
git submodule update
```

Each submodule uses its own Dockerfile:

- `./Dockerfile.make-api-fe`
- `./Dockerfile.make-api-be-fetcher`

You also **need to create Environment files** for each submodule, with a name `.env.tests.local`:

- `./make-api-fe/.env.tests.local` (new file)
- `./make-api-be-fetcher/.env.tests.local` (new file)

You can use `.env.example` files in each submodule as a template. But you need to make sure that the URLs in the environment files are correct.


### make-api-fe

Specific environment variables for make-api-fe:

| Variable                        | Modified value                                                                          | Description                                                                                                                          |
|---------------------------------|-----------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------|
| `DATABASE_URL`                  | `"postgresql://postgres:mysecretpassword@make-api-db-postgres:5432/mydb?schema=public"` | Database URL. Hostname must match the **service name** from the [docker-compose.yml](./docker-compose.yml) config.                   |
| `PAGE_FETCHER_SERVICE_BASE_URL` | "http://make-api-be-fetcher:4001"                                                       | URL of the URL Fetcher service. Hostname must match the **service name** from the [docker-compose.yml](./docker-compose.yml) config. |


### make-api-be-fetcher

Specific environment variables for make-api-be-fetcher:

| Variable                   | Modified value                     | Description                                                                                                                     |
|----------------------------|------------------------------------|---------------------------------------------------------------------------------------------------------------------------------|
| `USER_AGENT_STRING`        | `"MakeApiBot/1.0"`                 | User agent string used for requests.                                                                                            |
| `URL_CACHE_CONNECTION_URL` | `"redis://make-api-db-redis:6379"` | URL of the cache database. Hostname must match the **service name** from the [docker-compose.yml](./docker-compose.yml) config. | 


# Running the project

To run the project, you need to have [Docker](https://docs.docker.com/get-docker/) and [Docker Compose](https://docs.docker.com/compose/install/) installed.

Then you can run the project with the following command:

```bash
docker compose down && docker compose up --build --detach
```

This will download all the necessary images, build the images for the submodules and run the containers (names can be different).

| Container name                          | Description             |
|-----------------------------------------|-------------------------|
| `make-api-tests-make-api-fe-1`          | **make-api-fe**         |
| `make-api-tests-make-api-be-fetcher-1`  | **make-api-be-fetcher** |
| `make-api-tests-make-api-db-postgres-1` | **PostgreSQL database** |
| `make-api-tests-make-api-db-redis-1`    | **Redis database**      |
| `make-api-tests-make-api-maildev-1`     | **Maildev server**      |
| `make-api-tests-mock-app-1`             | **mock-app server**     |


After starting the containers, you need to set up the database. 
You can do that with the script [make-api-fe.init-db.sh](./make-api-fe.init-db.sh). 
It will create the database schema and seed the database with data (see the [seed.ts](./make-api-fe/prisma/seed.ts) file). 

You will be prompted to enter the database URL. Since the docker-compose is set up in a way that the **make-api-db-postgres** service exposes the PostgreSQL database on port 5432, you can use the following URL: `postgresql://postgres:mysecretpassword@localhost:5432/mydb?schema=public`.

```bash
./make-api-fe.init-db.sh
# Enter PostgresSQL connection URL: postgresql://postgres:mysecretpassword@localhost:5432/mydb?schema=public
```

After that, you can run should be able to access the application at [http://localhost:3000](http://localhost:3000).

You can use the following credentials to log in:

- **E-mail:** `admin@make.api`
- **Password:** `admin-password`

## Common errors

### Error: `make-api-be-fetcher` container exited with code 1

Issue: `make-api-be-fetcher` container exits with code 1 and the following error:

```
make-api-tests-make-api-be-fetcher-1   | ⚡️[server]: Server is running at http://localhost:4001
make-api-tests-make-api-be-fetcher-1   | node:events:492
make-api-tests-make-api-be-fetcher-1   |       throw er; // Unhandled 'error' event
make-api-tests-make-api-be-fetcher-1   |       ^
make-api-tests-make-api-be-fetcher-1   | 
make-api-tests-make-api-be-fetcher-1   | Error: connect ECONNREFUSED 127.0.0.1:6379
make-api-tests-make-api-be-fetcher-1   |     at TCPConnectWrap.afterConnect [as oncomplete] (node:net:1595:16)
make-api-tests-make-api-be-fetcher-1   | Emitted 'error' event on Keyv instance at:
make-api-tests-make-api-be-fetcher-1   |     at KeyvRedis.<anonymous> (/home/pptruser/node_modules/keyv/src/index.js:60:46)
make-api-tests-make-api-be-fetcher-1   |     at KeyvRedis.emit (node:events:514:28)
make-api-tests-make-api-be-fetcher-1   |     at EventEmitter.<anonymous> (/home/pptruser/node_modules/@keyv/redis/dist/index.js:31:48)
make-api-tests-make-api-be-fetcher-1   |     at EventEmitter.emit (node:events:514:28)
make-api-tests-make-api-be-fetcher-1   |     at EventEmitter.silentEmit (/home/pptruser/node_modules/ioredis/built/Redis.js:464:30)
make-api-tests-make-api-be-fetcher-1   |     at Socket.<anonymous> (/home/pptruser/node_modules/ioredis/built/redis/event_handler.js:196:14)
make-api-tests-make-api-be-fetcher-1   |     at Object.onceWrapper (node:events:629:26)
make-api-tests-make-api-be-fetcher-1   |     at Socket.emit (node:events:526:35)
make-api-tests-make-api-be-fetcher-1   |     at emitErrorNT (node:internal/streams/destroy:151:8)
make-api-tests-make-api-be-fetcher-1   |     at emitErrorCloseNT (node:internal/streams/destroy:116:3) {
make-api-tests-make-api-be-fetcher-1   |   errno: -111,
make-api-tests-make-api-be-fetcher-1   |   code: 'ECONNREFUSED',
make-api-tests-make-api-be-fetcher-1   |   syscall: 'connect',
make-api-tests-make-api-be-fetcher-1   |   address: '127.0.0.1',
make-api-tests-make-api-be-fetcher-1   |   port: 6379
make-api-tests-make-api-be-fetcher-1   | }
make-api-tests-make-api-be-fetcher-1   | 
make-api-tests-make-api-be-fetcher-1   | Node.js v20.9.0
make-api-tests-make-api-be-fetcher-1 exited with code 1
```

Solution: You need to set up correct URL for the Redis database in the `make-api-be-fetcher/.env.tests.local` file. The hostname must match the **service name** from the [docker-compose.yml](./docker-compose.yml) config.

```dotenv
# Previous value
URL_CACHE_CONNECTION_URL=redis://localhost:6379

# New value
URL_CACHE_CONNECTION_URL=redis://make-api-db-redis:6379
```

### Container `make-api-tests-make-api-fe-1` restarts after attempting to login

Login with a user at [http://localhost:3000/login](http://localhost:3000/login) fails.

Issue: `make-api-tests-make-api-fe-1` container uses `hash` function from the `bcrypt` library 
to hash the password, however the implementation uses different library on the host machine 
than in the container. This causes the hash to be different and the login fails. 
(See https://github.com/kelektiv/node.bcrypt.js/issues/708#issuecomment-468678405 for more info.)

Solution: You need to clear the `node_modules` directory in the `make-api-fe` submodule 
and let the container install the dependencies itself.

## Running tests

First you need to install the NPM dependencies for Playwright:

```bash
npm i
```

The e2e tests are located in the [./tests](./tests) directory. They are written in [Playwright](https://playwright.dev/). You can run them with the following command:

```bash
npx playwright test -ui
```
