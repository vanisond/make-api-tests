#!/bin/bash

script_dir="$( dirname "${BASH_SOURCE:-$0}" )"
cd "$script_dir/make-api-fe" # Need to be in the same directory as the package.json file

read -p "Enter PostgresSQL connection URL: " DATABASE_URL
export DATABASE_URL="$DATABASE_URL"

npx prisma generate    # Generate Prisma Client -- needed for seeding
npx prisma db push     # Create database tables
npx prisma db seed     # Seed database with initial data
