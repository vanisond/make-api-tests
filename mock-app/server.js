const express = require('express');
const app = express();
const port = process.env.PORT;

if (!port) {
    console.error('PORT environment variable not set');
    process.exit(1);
}

// Serve files from the 'public' directory
app.use(express.static('public'));

app.set('etag', false); // turn off etags

app.use((req, res, next) => {
    next();
    const timestamp = new Date().toISOString();
    const method = req.method;
    const url = req.url;
    const status = res.statusCode;
    console.log(`[${timestamp}]: ${method} ${status} ${url}`);
})

// Example of a route that returns given status
app.get('/status/:code', (req, res) => {
    const code = parseInt(req.params.code);
    if (code < 100 || code > 599) {
        res.status(400).send("Invalid status code");
        return;
    }
    res.status(parseInt(req.params.code, 10)).send(`Status code: ${req.params.code}`);
});

app.get('/cache/etag', (req, res) => {
    res.set('ETag', '"12345"');
    res.send('Cached content demo with ETag');
});

app.get('/cache/cache-control', (req, res) => {
    res.set('Cache-Control', 'public, max-age=10');
    res.send('Cached content demo with Cache-Control and max-age set to 10 seconds');
});

app.get('/cache/last-modified', (req, res) => {
    // if If-Modified-Since header is under 10 seconds old, return 304
    const ifModifiedSince = req.get('If-Modified-Since');
    if (ifModifiedSince) {
        const lastModified = new Date(ifModifiedSince);
        const now = new Date();
        const seconds = (now.getTime() - lastModified.getTime()) / 1000;
        if (seconds < 10) {
            res.status(304).send();
            return;
        }
    }

    // otherwise, return 200 and set Last-Modified header to now
    res.set('Last-Modified', new Date().toUTCString());
    res.send('Cached content demo with Last-Modified header');
});

app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}`);
});
