import {expect, Locator, Page} from "@playwright/test";
import {ProjectPage} from "./project-page";

export class ProjectsPage {
    readonly page: Page;
    readonly createProjectButton: Locator;
    constructor(page: Page) {
        this.page = page;
        this.createProjectButton = page.getByRole('button', { name: 'Create project' }).first();
    }
    async goto() {
        await this.page.goto('/projects');
    }
    async createProject({projectName}: { projectName: string }) {
        // Open create dialog
        await this.createProjectButton.click();
        // Fill in project name and submit
        await this.page.getByLabel('Project name').fill(projectName);
        await this.page.getByLabel('Create new project')
            .getByRole('button', { name: 'Create project' })
            .click();
        // wait for project to be created
        await expect(this.page.getByRole('link', { name: projectName })).toBeVisible();
        return {projectName};
    }
    async removeProject(projectName: string) {
        await expect(this.page.getByRole('link', { name: projectName })).toBeVisible();
        await this.page.getByRole('row', { name: new RegExp(projectName) }).getByRole('button').click();
        await this.page.getByRole('button', { name: 'Delete' }).click();
        // Check if project was removed
        await expect(this.page.getByRole('link', { name: projectName })).toBeHidden();
    }
    async openProject(projectName: string) {
        await this.page.getByRole('link', { name: projectName }).click();
        return new ProjectPage(this.page);
    }
}
