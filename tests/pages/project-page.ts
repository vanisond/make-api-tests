import {expect, Page} from "@playwright/test";
import {DocumentPage} from "./document-page";

export class ProjectPage {
    readonly page: Page;

    constructor(page: Page) {
        this.page = page;
    }

    async openSettings() {
        await this.page.getByRole('button', {name: 'Project settings'}).click();
        await expect(this.page.getByLabel('Project settings')).toBeVisible();
    }

    async closeSettings() {
        await this.page.getByRole('button', {name: 'Close'}).click();
    }

    async updateProject({projectName}: { projectName: string }) {
        await this.openSettings();
        // Open create dialog
        // Fill in project name and submit
        await this.page.getByLabel('Project name').fill(projectName);
        await this.page.getByLabel('Project settings')
            .getByRole('button', {name: 'Save changes'})
            .click();
        await this.closeSettings();
        // wait for project to be created
        await expect(this.page.getByRole('heading', {name: projectName})).toBeVisible();
        return {projectName};
    }

    async addAccessToken({tokenName}: { tokenName: string }) {
        await this.openSettings();
        await this.page.getByRole('button', {name: 'Add token'}).click();
        await expect(this.page.getByLabel('Create access token')).toBeVisible();

        await this.page.getByLabel('Description').fill(tokenName);
        // copy value from a filled text field
        const accessToken = await this.page.getByPlaceholder('************').inputValue();
        await this.page.getByRole('button', {name: 'Save token'}).click();

        // Wait to see the token in the list
        await expect(this.page.getByRole('cell', { name: tokenName })).toBeVisible();
        await this.closeSettings();
        return {tokenName, accessToken};
    }

    async setAccessTokenRateLimit({tokenName, maxRequests, timeWindowMs}: {
        tokenName: string,
        maxRequests: number,
        timeWindowMs: number
    }) {
        await this.openSettings();
        await this.page.getByRole('row', {name: new RegExp(tokenName)})
            .getByRole('button', {name: 'Limits'})
            .click();
        await expect(this.page.getByRole('heading', {name: 'Rate limits'})).toBeVisible();
        await this.page.getByLabel('Maximum requests per time').fill(`${maxRequests}`);
        await this.page.getByLabel('Time window (milliseconds)').fill(`${timeWindowMs}`);
        await this.page.getByRole('button', { name: 'Save rate limit' }).click();
        await expect(this.page.getByText('Rate limit updated!')).toBeVisible();
        await this.closeSettings();
    }

    async addDocument({url, documentName}: { url: string, documentName: string }) {
        await this.page.getByRole('button', {name: 'Add document'}).first().click();
        await expect(this.page.getByLabel('Add document')).toBeVisible();

        await this.page.getByLabel('Document URL').fill(url);
        await this.page.getByLabel('Document name').fill(documentName);
        await this.page.getByLabel('Add document').getByRole('button', {name: 'Add document'}).click();
        await expect(this.page.getByRole('link', {name: documentName})).toBeVisible();
        return {url, documentName};
    }

    async removeDocument(documentName: string) {
        await expect(this.page.getByRole('link', {name: documentName})).toBeVisible();
        await this.page.getByRole('row', {name: new RegExp(documentName)})
            .getByRole('button', { name: 'Delete' })
            .click();
        await this.page.getByLabel('Are you absolutely sure?')
            .getByRole('button', {name: 'Delete'})
            .click();
        await expect(this.page.getByRole('link', {name: documentName})).toBeHidden();
    }

    async getApiLink({documentName, accessToken}: { documentName: string, accessToken: string }) {
        // Open API endpoint dialog
        await expect(this.page.getByRole('link', {name: documentName})).toBeVisible();
        await this.page.getByRole('row', {name: new RegExp(documentName)})
            .getByRole('button', {name: 'API'})
            .click();

        // FIll in the access token
        await expect(this.page.getByLabel('Access token')).toBeVisible();
        await expect(this.page.getByLabel('Transformation endpoint')).toBeVisible();
        await this.page.getByLabel('Access token').fill(accessToken);
        const apiLink = await this.page.getByLabel('Transformation endpoint').inputValue();

        // Close dialog
        await this.page.getByRole('button', {name: 'Close'}).click();
        return apiLink;
    }

    async openDocument(documentName: string) {
        await this.page.getByRole('link', {name: documentName}).click();
        return new DocumentPage(this.page);
    }
}
