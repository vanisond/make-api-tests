import {expect, Locator, Page} from "@playwright/test";

export class DocumentPage {
    readonly page: Page;
    constructor(page: Page) {
        this.page = page;
    }

    async updateDocument({url, documentName}: { url: string, documentName: string }) {
        await this.page.getByRole('button', {name: 'Document settings'}).first().click();
        await expect(this.page.getByLabel('Document settings')).toBeVisible();

        await this.page.getByLabel('Document URL').fill(url);
        await this.page.getByLabel('Document name').fill(documentName);
        await this.page.getByLabel('Document settings').getByRole('button', {name: 'Update document'}).click();

        await expect(this.page.getByText('Document updated!')).toBeVisible();
        await expect(this.page.getByRole('heading', { name: new RegExp(documentName) })).toBeVisible();
        await expect(this.page.getByText(url)).toBeVisible();

        return {url, documentName};
    }

    async saveChanges() {
        await this.page.getByRole('button', { name: 'Save changes' }).click();
        await expect(this.page.getByText('Schema saved!')).toBeVisible();
    }

    async setH1Transformation() {
        // Open the "type" select of the first node
        await this.page.getByRole('combobox').first().click();
        // Select the "string" option
        await this.page.getByLabel('string').first().click();
        // set the selector to "h1¨, so the transformation output should be a string containing the h1 text
        await this.page.getByPlaceholder('CSS Selector...').fill('h1');
    }
}
