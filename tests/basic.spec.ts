import {test} from "./loggedUserTest";
import {ProjectsPage} from "./pages/projects-page";
import {expect} from "@playwright/test";

// Should equal the TRANSFORM_FAILURE_DISABLE_DOCUMENT_TIMEOUT_MS env variable
const disableDocumentMs = 1000 * 5; // 5 seconds

test('Has projects page', async ({page, account}) => {
    const projectsPage = new ProjectsPage(page);
    await projectsPage.goto();
    await expect(page.getByRole('heading', {name: 'Projects'})).toBeVisible();
    await expect(page.getByRole('button', {name: 'Create project'}).first()).toBeVisible();
});

test('Create and remove project', async ({page, account}) => {
    const projectsPage = new ProjectsPage(page);
    await projectsPage.goto();
    const {projectName: newProjectName} = await projectsPage.createProject({projectName: 'Test project'});

    // Open project and update it
    const projectPage = await projectsPage.openProject(newProjectName);
    const {projectName: updatedProjectName} = await projectPage.updateProject({projectName: 'Test project updated'});

    // Add and remove document
    const {documentName} = await projectPage.addDocument({
        url: 'http://mock-app:8080/samples/basic.html',
        documentName: 'Basic sample'
    });
    await projectPage.removeDocument(documentName);

    // Remove project
    await projectsPage.goto();
    await projectsPage.removeProject(updatedProjectName);
});

test('Access transformation via an API endpoint URL', async ({page, request}) => {
    const projectsPage = new ProjectsPage(page);
    await projectsPage.goto();
    const {projectName} = await projectsPage.createProject({projectName: 'Test project'});

    // Open project and add access token
    const projectPage = await projectsPage.openProject(projectName);
    const {tokenName, accessToken} = await projectPage.addAccessToken({tokenName: 'Test token'});
    const {documentName} = await projectPage.addDocument({
        url: 'http://mock-app:8080/samples/basic.html',
        documentName: 'Basic sample'
    });

    // get the link for a document
    const apiLink = await projectPage.getApiLink({documentName, accessToken});
    console.log("API Endpoint URL: ", apiLink);

    // Set a sample transformation of the <h1> element
    const document = await projectPage.openDocument(documentName);
    await document.setH1Transformation();
    await document.saveChanges();

    // Send a GET request to the API endpoint
    const response = await request.get(apiLink);
    expect(response.status()).toBe(200);

    // Check a specific header
    const contentType = response.headers()['content-type'];
    expect(contentType).toContain('application/json');

    // Check response body
    const responseBody = await response.json();
    expect(responseBody).toHaveProperty('data', "Basic HTML page");

    // cleanup
    await projectsPage.goto();
    await projectsPage.removeProject(projectName);
});

test('Disable transformation of an invalid document', async ({page, request}) => {
    const projectsPage = new ProjectsPage(page);
    await projectsPage.goto();
    const {projectName} = await projectsPage.createProject({projectName: 'Test project'});

    // Open project and add access token
    const projectPage = await projectsPage.openProject(projectName);
    const {tokenName, accessToken} = await projectPage.addAccessToken({tokenName: 'Test token'});
    const {documentName} = await projectPage.addDocument({
        url: 'http://mock-app:8080/status/500',
        documentName: 'HTTP 500 Test Page'
    });

    // get the link for a document
    const apiLink = await projectPage.getApiLink({documentName, accessToken});

    // Send a GET request to the API endpoint
    const response1 = await request.get(apiLink);
    expect(response1.status()).toBe(500);

    // Check response body
    const responseBody = await response1.json();
    expect(responseBody).toEqual({
        "success": false,
        "message": "Failed to fetch HTML for the document: http://mock-app:8080/status/500"
    });

    // wait for the document to be disabled
    await page.waitForTimeout(disableDocumentMs);
    const response2 = await request.get(apiLink);
    expect(response2.status()).toBe(500);

    // Document should be disabled in the following request
    const response3 = await request.get(apiLink);
    expect(response3.status()).toBe(503);

    // Fix the document
    const document = await projectPage.openDocument(documentName);
    await document.updateDocument({url: 'http://mock-app:8080/status/200', documentName: 'HTTP 200 Test Page'});
    await document.saveChanges();

    // Check if the document is enabled again
    const response4 = await request.get(apiLink);
    expect(response4.status()).toBe(200);

    // cleanup
    await projectsPage.goto();
    await projectsPage.removeProject(projectName);
});

test('Rate-limit the API endpoint', async ({page, request}) => {
    const maxRequests = 10;
    const timeWindowMs = 5 * 1000;

    const projectsPage = new ProjectsPage(page);
    await projectsPage.goto();
    const {projectName} = await projectsPage.createProject({projectName: 'Test Project - rate-limiting'});

    // Open project and add access token
    const projectPage = await projectsPage.openProject(projectName);
    const {tokenName, accessToken} = await projectPage.addAccessToken({tokenName: 'Test token'});
    await projectPage.setAccessTokenRateLimit({tokenName, maxRequests, timeWindowMs});
    const {documentName} = await projectPage.addDocument({
        url: 'http://mock-app:8080/samples/basic.html',
        documentName: 'Basic sample'
    });

    // get the link for a document
    const apiLink = await projectPage.getApiLink({documentName, accessToken});

    // Send a GET request to the API endpoint
    for (let i = 0; i < maxRequests; i++) {
        const response = await request.get(apiLink);
        expect(response.status()).toBe(200);
    }

    // The next request should fail
    const response = await request.get(apiLink);
    expect(response.status()).toBe(429);

    // wait for the rate-limit to reset
    await page.waitForTimeout(timeWindowMs);
    const response2 = await request.get(apiLink);
    expect(response2.status()).toBe(200);

    // cleanup
    await projectsPage.goto();
    await projectsPage.removeProject(projectName);
});
