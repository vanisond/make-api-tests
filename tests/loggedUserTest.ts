import config from '../playwright.config';
import {expect, test as base} from '@playwright/test';

type Account = {
    email: string;
    password: string;
};
// Note that we pass worker fixture types as a second template parameter.
export const test = base.extend<{}, { account: Account }>({
    account: [async ({browser}, use, workerInfo) => {
        // Unique email.
        const random = Math.floor(Math.random() * 100000);
        const email = `user${workerInfo.workerIndex}-${random}@example.com`;
        const password = 'verysecure';
        // Create the account with Playwright.
        const page = await browser.newPage();
        await page.goto('/register');
        await expect(page.getByLabel('E-mail', {exact: true})).toBeVisible();
        await page.getByLabel('E-mail', {exact: true}).click();
        await page.getByLabel('E-mail', {exact: true}).fill(email);
        await page.getByLabel('Password', {exact: true}).click();
        await page.getByLabel('Password', {exact: true}).fill(password);
        await page.getByLabel('Confirm password').click();
        await page.getByLabel('Confirm password').fill(password);
        await page.getByRole('button', {name: 'Sign Up'}).click();
        // Make sure everything is ok.
        await expect(page.getByRole('status')).toHaveText(/Account created!/);
        // Do not forget to cleanup.
        await page.close();
        // Use the account value.
        await use({email, password});

        // Delete the account after the test.
        const cleanupPage = await browser.newPage();
        await cleanupPage.goto(`${config.use.baseURL}/login`);
        await cleanupPage.getByLabel('E-mail').fill(email);
        await cleanupPage.getByLabel('Password').fill(password);
        await cleanupPage.getByRole('button', {name: 'Sign In'}).click();
        await expect(cleanupPage.getByText('login')).toBeHidden();
        await expect(cleanupPage.getByText('user')).toBeVisible();

        await cleanupPage.goto(`${config.use.baseURL}/profile/delete-account`);
        await expect(cleanupPage.getByRole('button', { name: 'Delete user' })).toBeVisible();
        await cleanupPage.getByRole('button', {name: 'Delete user'}).click();
        await cleanupPage.getByPlaceholder('Type DELETE to confirm.').click();
        await cleanupPage.getByPlaceholder('Type DELETE to confirm.').fill('DELETE');
        await cleanupPage.getByRole('button', {name: 'Delete'}).click();
        await cleanupPage.close();

    }, {scope: 'worker'}],
    page: async ({page, account}, use) => {
        // Sign in with our account.
        const {email, password} = account;
        await page.goto('/login');
        await page.getByLabel('E-mail').fill(email);
        await page.getByLabel('Password').fill(password);
        await page.getByRole('button', {name: 'Sign In'}).click();
        await expect(page.getByText('login')).toBeHidden();
        await expect(page.getByText('user')).toBeVisible();
        // Use signed-in page in the test.
        await use(page);
    },
});
export {expect} from '@playwright/test';
